/**
 * Factory Function for confluence-google-calendar add-on.
 */
function makeServer() {
    var express = require('express');
    var ac = require('atlassian-connect-express');
    var hbs = require('express-hbs');
    var http = require('http');
    var path = require('path');
    var os = require('os');
    
    var staticDir = path.join(__dirname, 'public');
    var viewsDir = __dirname + '/views';
    var routes = require('./routes');
    var app = express();
    var addon = ac(app);
    var port = addon.config.port();
    var devEnv = app.get('env') == 'development';
    
    app.set('port', port);
    
    // Configure Handlebars
    app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
    app.set('view engine', 'hbs');
    app.set('views', viewsDir);
    
    // Configure Middleware
    
    // Init ACE 
    app.use(addon.middleware());
    app.use(express.static(staticDir));
    
    routes(app, addon);
    
    // Boot
    var server = http.createServer(app).listen(port, function() {
        console.log('Add-on servefr running at http://' + os.hostname() + ':' + port);
    
        if (devEnv) addon.register();
    });

    return server;
}

module.exports = makeServer;

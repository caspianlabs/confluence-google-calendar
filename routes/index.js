module.exports = function (app, addon) {
    app.get('/', function (req, res) {
        res.format({
            'text/html': function() {
                res.redirect('/atlassian-connect.json');
            },
            'application/json': function() {
                res.redirect('/atlassian-connect.json');
            }
        });
    });
    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync('routes');
        for (var index in files) {
            var file = files[index];
            if (file === 'index.js') continue;
            if (path.extname(file) != '.js') continue;

            var routes = require('./' + path.basename(file));

            if (typeof routes === 'function') {
                routes(app, addon);
            }
        }
    }
};
